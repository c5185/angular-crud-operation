
const mysql = require('mysql2');

function getMQLConnection() {

    // create the connection to database
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        database: 'angularcrudoperation',
        password: 'root'
    });

 
     connection.connect((err)=>{
        if(err){
            console.log(err);}
            else{
       
            console.log('Connected to database');;
        }
    });
    return connection;
     
}


module.exports = { getMQLConnection };;