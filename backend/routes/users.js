var express = require('express');
const { getMQLConnection } = require('../config/mySqlConnection');
var router = express.Router();

/* GET users  . */
router.get('/', function (req, res, next) {
  try {

console.log(getMQLConnection());  

    // simple query
    getMQLConnection().query(
      `SELECT * FROM users`,
      function (err, results, fields) {
        if (err) {
          console.log(err);
          res.send(err);
        }
        if (results.length>0) {
          return res.status(200).json({status:true,data:results});
        }else{
          return res.status(404).json({status:false, message: 'No users found'});
        }
      }
    );

  } catch (error) {
    console.log(error);
  }
});


/* GET specific user by id  . */
router.get('/:id', function (req, res, next) {
  try {
     getMQLConnection().query(
      `SELECT * FROM users WHERE id=${req.params.id}`,
      function (err, results, fields) {
        if (err) {
          console.log(err);
          res.send(err);
        }
        if (results.length>0) {
          return res.status(200).json({status:true,data:results});
        }else{
          return res.status(404).json({status:false,message:"User not found"});
        }
      }
    );

  } catch (error) {
    console.log(error);
  }


});

/* DELETE specific user by id  . */
router.delete('/:id', function (req, res, next) {
  try {
    const id = req.params.id;
     getMQLConnection().execute(
      `DELETE FROM users WHERE id=?`,
      [id],
      function (err, results, fields) {
        if (err) {
          console.log(err);
          res.send(err);
        }
        if (results.affectedRows>0) {
          return res.status(200).json({status:true, message:"User deleted successfully"});
        }else{
          return res.status(404).json({status:false,message:"User not deleted"});
        }
      }
    );

  } catch (error) {
    console.log(error);
  }
});

/* POST add new user data  . */
router.post('/add', function (req, res, next) {
  try {

    console.log(req.body);
    getMQLConnection().execute(
      `INSERT INTO users (fullname,email,mobile,gender) VALUES ('${req.body.fullname}','${req.body.email}','${req.body.mobile}','${req.body.gender}')`,
      function (err, results, fields) {
        if (err) {
          console.log(err);
          res.send(err);
        }
        if (results.affectedRows>0) {
          return res.status(200).json( {status:true, message:"User added successfully"});
        }
        else{
          return res.status(404).json({status:false,message:"something went wrong"});
        }
      }
    );

  } catch (error) {
    console.log(error);
  }
});


/* UPDATE specific user by id  . */
router.put('/:id', function (req, res, next) {
  try {
    const { fullname, email,mobile, gender, } = req.body;
    let id = req.params.id;
    const query=  `UPDATE users  SET  fullname =?, email = ?, mobile = ?, gender = ? WHERE id = ?`;
     getMQLConnection().execute(query,
      [fullname,email,mobile,gender , id],
      function (err, results, fields) {
        if (err) {
          console.log(err);
          res.send(err);
        }
        if (results.affectedRows>0) {
          return res.status(200).json( {status:true, message:"User updated successfully"});
        }else{
          return res.status(404).json({status:false,message:"User not found"});
        }
      }
    );

  } catch (error) { 
    console.log(error);
  }
});


module.exports = router;